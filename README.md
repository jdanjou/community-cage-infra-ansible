# Community Cage Infrastructure Management using Ansible

## Introduction

We offer a range of full-lifecycle services for open source community projects that are strategic to Red Hat. Services range from managed hosting to server colocation (ping/power/pipe/racks) in multiple data centers. We also provide project planning & execution, budget planning, and purchasing support for tenants.

This is a physical co-location, managed + virtual services including private cloud, and public cloud offering from the Open Source and Standards (OSAS) working with Regional IT, ACS, and PnT DevOps.

This repository contains Ansible rules to manage shared services and VM offering for all tenants of the cage.

You need Ansible >=2.1.2.0~rc3 to be able to handle the YAML-based 'hosts' file format.

## Admin-specific Production Settings

You can use `group_vars/all/local_settings.yml` for you local
settings like `ansible_become_pass` if your computer storage is
encrypted. Use `--ask-sudo-pass` if you don't want to use this
method. Currently Ansible is unable to ask _when needed_ so
the global setting has been disabled in `ansible.cfg`.

